<?php
/**
 * (c) MagnaX Software
 */

namespace Drupal\google_places_api;

use Drupal;
use Drupal\Core\Extension\MissingDependencyException;
use SynapseLink\GooglePlacesAPI;

class GooglePlacesAPIFactory {

  /**
   * @var bool
   */
  protected static $isLoaded;

  /**
   * @var array
   */
  protected static $library;

  /**
   * @return bool
   */
  public static function isGooglePlacesAPILoaded() {
    return static::$isLoaded;
  }

  public static function loadGooglePlacesAPI() {
    return static::$isLoaded = (static::$library = libraries_load('google-places-api')) && static::$library['loaded'] !== FALSE;
  }

  /**
   * @return \google_places_api\google_places_api
   * @throws \Drupal\Core\Extension\MissingDependencyException
   */
  public static function createaGooglePlacesAPIFromSettings() {

    // Check Class Availability
    if ( false ) {
      throw new MissingDependencyException('Could not load the Google Places API library: ' . static::$library['error message']);
    }

    $config = Drupal::config('google_places_api.settings');

    // Retrieve API Key
    $api_key = $config->get('google_places_api_key');

    // Retrieve Proxy Information
    if (false) {
      $request_proxy = [];
    } 
    
    // Initialize GooglePlacesAPI Object
    $google_places_api = new GooglePlacesAPI($api_key, $request_proxy);

    return $google_places_api;

  }
}
