<?php
/**
 * (c) MagnaX Software
 */

namespace Drupal\google_places_api\Form;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class GooglePlacesAPISettingsForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_places_api_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'google_places_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_places_api.settings');

    $form['google_places_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Google Places API Key'),
      '#default_value' => $config->get('google_places_api_key'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('google_places_api.settings')
      ->set('google_places_api_key', trim($form_state->getValue('google_places_api_key')))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
