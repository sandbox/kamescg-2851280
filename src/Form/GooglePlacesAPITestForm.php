<?php


namespace Drupal\google_places_api\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use SynapseLink\GooglePlaces\GooglePlacesAPI;
use SynapseLink\GooglePlaces\GoogleGeocoding;

class GooglePlacesAPITestForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'google_places_api_admin_test';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_places_api.settings');
    $google_places_api_key = $config->get('google_places_api_key');

    if (empty($google_places_api_key)) {
      $form['container']['#type'] = 'container';
      $form['container']['notice'] = array(
        '#markup' => $this->t('Please specify the Google Places API Key.'),
      );
    }


    $form['actions']['#type'] = 'actions';
    $form['actions']['#weight'] = -1;
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Run Test'),
      '#button_type' => 'primary',
      '#disabled' => (empty($google_places_api_key)),
    );


  

    // By default, render the form using theme_system_config_form().
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('google_places_api.settings');
    $google_places_api_key = $config->get('google_places_api_key');

    // Check API Credentials Exist
    if (empty($google_places_api_key)) {
      drupal_set_message($this->t('Please specify the Google Places API Key.'), 'error');
      return;
    }

    // Initialize Object :: Check Google Places API Key exists
    if ( !empty($google_places_api_key)) {
      $GooglePlacesAPI = new GooglePlacesAPI($google_places_api_key);
      $latitude = '37.5947657';
      $longitude = '-122.3699911';
      $GooglePlacesAPI->setLocation($latitude . ',' . $longitude);

      $GooglePlacesAPITest = $GooglePlacesAPI->nearbySearch();

      if (is_array($GooglePlacesAPITest['result']))
        drupal_set_message($this->t('Succesfully connected to Google Places API.'), 'status');
      } else {
        drupal_set_message($this->t('The Google Places API is not currently working.'), 'error');
        return;
    }

    $form_state->setRebuild(TRUE);

  }
}
