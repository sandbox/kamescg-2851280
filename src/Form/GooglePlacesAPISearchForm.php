<?php


namespace Drupal\google_places_api\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use SynapseLink\GooglePlaces\GooglePlacesAPI;
use SynapseLink\GooglePlaces\GoogleGeocoding;

class GooglePlacesAPISearchForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'google_places_api_admin_test';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_places_api.settings');
    $google_places_api_key = $config->get('google_places_api_key');

    if (empty($google_places_api_key)) {
      $form['container']['#type'] = 'container';
      $form['container']['notice'] = array(
        '#markup' => $this->t('Please specify the Google Places API Key.'),
      );
    }

    $form['#attached'] = array(
        'library' => array(
          'google_places_api/google_places_api.search',
          'google_places_api/google_places_api.chosen'
        )
      );

    /* --- Search :: Container --- */
    $form['google_places_search_container'] = array(
      '#type' => 'fieldset',
      '#open' => TRUE,
      '#title' => $this->t('Search'),
      '#attached' => array(
        'library' => array(
          'drupal/jquery'
          // 'google_places_api/google_places_api.chosen'
        )
      )
    );

      /* --- Keyword --- */
      $form['google_places_search_container']['keyword'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Keyword(s)'),
        '#title' => $this->t('Enter keyword(s) to refine search.'),
        '#size' => 50,
        '#attributes' => [
          'class' => 'field--keyword',
          'placeholder' => 'Car Mechanic'
        ]
      );

      /* --- Category --- */
      $form['google_places_search_container']['types'] = array(
        '#type' => 'select',
        '#title' => $this->t('Types'),
        '#multiple' => FALSE,
        '#size' => 10,
        '#options' => [
          'accounting' => 'Account',
          'airport' => 'Airport',
          'amusement_park' => 'Amusement Park',
          'aquarium' => 'Aquarium',
          'art_gallery' => 'Art Gallery',
          'atm' => 'ATM',
          'bakery' => 'Bakery',
          'bank' => 'Bank',
          'bar' => 'Bar',
          'beauty_salon' => 'Beauty Salon', 
          'bicycle_store' => 'Bicycle Store', 
          'book_store' => 'Book Store', 
          'bowling_alley' => 'Bowling Alley', 
          'bus_station' => 'Bus Station', 
          'cafe' => 'Cafe', 
          'campground' => 'Campground', 
          'car_dealer' => 'Car Dealer', 
          'car_rental' => 'Car Rental', 
          'car_repair' => 'Car Repair', 
          'car_wash' => 'Car Wash', 
          'casino' => 'Casino', 
          'cemetery' => 'Cemetery', 
          'church' => 'Church', 
          'city_hall' => 'City Hall', 
          'clothing_store' => 'Clothing Store', 
          'convenience_store' => 'Convenience Store', 
          'courthouse' => 'Courthouse', 
          'dentist' => 'Dentist', 
          'department_store' => 'Department Store', 
          'doctor' => 'Doctor', 
          'electrician' => 'Electrician', 
          'electronics_store' => 'Electronics Store', 
          'embassy' => 'Embassy', 
          'establishment (deprecated)' => 'Establishment', 
          'finance (deprecated)' => 'Finance', 
          'fire_station' => 'Fire Station', 
          'florist' => 'Florist', 
          'food (deprecated)' => 'Food', 
          'funeral_home' => 'Funeral Home', 
          'furniture_store' => 'Furniture Store', 
          'gas_station' => 'Gas Station', 
          'general_contractor (deprecated)' => 'General Contractor', 
          'grocery_or_supermarket (deprecated)' => 'Grocery/Supermarket', 
          'gym' => 'Gym', 
          'hair_care' => 'Hair Care', 
          'hardware_store' => 'Hardware Store', 
          'health (deprecated)' => 'Health', 
          'hindu_temple' => 'Hindu Template', 
          'home_goods_store' => 'Home Goods Store', 
          'hospital' => 'Hos[ital', 
          'insurance_agency' => 'Insurance Agency', 
          'jewelry_store' => 'Jewelery Store', 
          'laundry' => 'Laundry', 
          'lawyer' => 'Lawyer', 
          'library' => 'Library', 
          'liquor_store' => 'Liquor Store', 
          'local_government_office' => 'Local Government Office', 
          'locksmith' => 'Locksmith', 
          'lodging' => 'Lodging', 
          'meal_delivery' => 'Meal Delivery', 
          'meal_takeaway' => 'Meal Takeaway', 
          'mosque' => 'Mosque', 
          'movie_rental' => 'Movie Rental', 
          'movie_theater' => 'Movie Theater', 
          'moving_company' => 'Moving Company', 
          'museum' => 'Museum', 
          'night_club' => 'Night Club', 
          'painter' => 'Painter', 
          'park' => 'Park', 
          'parking' => 'Parking', 
          'pet_store' => 'Pet Store', 
          'pharmacy' => 'Pharmacy', 
          'physiotherapist' => 'Physiotherapist', 
          'place_of_worship (deprecated)' => 'Place of Worship', 
          'plumber' => 'Plumber', 
          'police' => 'Police', 
          'post_office' => 'Post Office', 
          'real_estate_agency' => 'Real Estate Agency', 
          'restaurant' => 'Resteraunt', 
          'roofing_contractor' => 'Roofing Contractor', 
          'rv_park' => 'RV Park', 
          'school' => 'School', 
          'shoe_store' => 'Shoe Store', 
          'shopping_mall' => 'Shopping Mall', 
          'spa' => 'Spa', 
          'stadium' => 'Stadium', 
          'storage' => 'Storage', 
          'store' => 'Store', 
          'subway_station' => 'Subway Station', 
          'synagogue' => 'Synagogue', 
          'taxi_stand' => 'Taxi Stand', 
          'train_station' => 'Train Station', 
          'transit_station' => 'Transit Station', 
          'travel_agency' => 'Travel Agency', 
          'university' => 'University', 
          'veterinary_care' => 'Veterninary Care', 
          'zoo' => 'Zoo'
        ]
      );

      /* --- Ranked By --- */
      $form['google_places_search_container']['ranked_by'] = array(
        '#type' => 'radios',
        '#title' => $this->t('Rank By'),
        '#size' => 40,
        '#options' => [
          'prominence' => 'Prominence',
          'distance' => 'Distance',
        ]
      );

    /* --- Address :: Container --- */
    $form['google_places_address_container'] = array(
      '#type' => 'fieldset',
      '#open' => TRUE,
      '#title' => $this->t('Address'),
    );

      /* --- Street Address --- */
      $form['google_places_address_container']['street_address'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Street Address'),
        '#size' => 40,
        '#attributes' => [
          'placeholder' => '3301 Lyon St'
        ]
      );

      /* --- City --- */
      $form['google_places_address_container']['city'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('City'),
        '#size' => 40,
        '#attributes' => [
          'placeholder' => 'San Francisco'
        ]
      );

      /* --- State --- */
      $form['google_places_address_container']['state'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('State'),
        '#size' => 5,
        '#attributes' => [
          'placeholder' => 'CA'
        ]
      );

      /* --- State --- */
      $form['google_places_address_container']['zip'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('ZIP'),
        '#size' => 10,
        '#attributes' => [
          'placeholder' => '94123'
        ]
      );

      /* --- Radius --- */
      $form['google_places_address_container']['radius'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Radius'),
        '#size' => 10,
        '#attributes' => [
          'placeholder' => '5000'
        ]
      );


    $form['actions']['#type'] = 'actions';
    $form['actions']['#weight'] = -1;
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Search Google Places'),
      '#button_type' => 'primary',
      '#disabled' => (empty($google_places_api_key)),
    );


    /* ------------------------------ */
    // Table Header
    /* ------------------------------ */
    $header = array(
      'rating' => array(
        'data' => $this->t('Rating'),
        'field' => 'w.rating',
        'class' => array(RESPONSIVE_PRIORITY_LOW)
      ),
      'name' => array(
        'data' => $this->t('Business'),
        'field' => 'w.form_name',
        'class' => array(RESPONSIVE_PRIORITY_MEDIUM)
      ),
      'address' => array(
        'data' => $this->t('Address'),
        'field' => 'w.address',
        'class' => array(RESPONSIVE_PRIORITY_LOW)
      ),
      'phone' => array(
        'data' => $this->t('Phone'),
        'field' => 'w.reference',
        'sort' => 'desc',
        'class' => array(RESPONSIVE_PRIORITY_MEDIUM)
      ),
      'website' => array(
        'data' => $this->t('Website'),
        'field' => 'w.reference',
        'sort' => 'desc',
        'class' => array(RESPONSIVE_PRIORITY_HIGH)
      ),
    );

    /* ------------------------------ */
    // Table Rows
    /* ------------------------------ */
    $rows = [];
    $storage = $form_state->getStorage();
    if (!empty($storage)) {

      foreach ($storage['result'] as $result_key => $result_value) {

        if($result_key != 'address_fixed') {

          // Initialize Object :: Check Google Places API Key exists
          if ( !empty($google_places_api_key)) {
            $GooglePlacesAPIBusiness = new GooglePlacesAPI($google_places_api_key);
          } else {
            drupal_set_message($this->t('The Google Places API Key must be set before sending a request.'), 'error');
            return;
          }

          $place_id =  $result_value['place_id'];

          // Set Object Properties :: Place Identification Number
          $GooglePlacesAPIBusiness->setPlaceId($place_id);
          // Request :: Google Maps API Business Details
          $details = $GooglePlacesAPIBusiness->details();

          // Extraction :: Pagination Request
          $name =    $result_value['name'];
          $address = $result_value['vicinity'];
          $rating =  $result_value['rating'];

          // Extraction :: Details Request
          $phone =   $details['result']['formatted_phone_number'];
          $website = $details['result']['website'];

          $rows[] = array(
            'data' => array(
              'rating' => array('data' => $rating ),
              'name' => array('data' => $name ),
              'address' => array('data' => $address),
              'phone' => array('data' => $phone),
              'website' => array('data' => $website),
            ),
            'class' => array( 'form--type' ),
          );
        }

      }

    }

    /* ------------------------------ */
    // Table Render Array
    /* ------------------------------ */
    $form['results_table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => array(
        'id' => 'admin-synapse', 
        'class' => array('admin-synapse')
        ),
      '#empty' => $this->t('No Businesses to display.'),
      '#weight' => 99
    );

    // By default, render the form using theme_system_config_form().
    $form['#theme'] = 'system_config_form';

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('google_places_api.settings');
    $google_places_api_key = $config->get('google_places_api_key');

    // Check API Credentials Exist
    if (empty($google_places_api_key)) {
      drupal_set_message($this->t('Please specify the Google Places API Key.'), 'error');
      return;
    }

    // Initialize Object :: Check Google Places API Key exists
    if ( !empty($google_places_api_key)) {
      $GoogleGeocoding = new GoogleGeocoding($google_places_api_key);
    } else {
      drupal_set_message($this->t('The Google Places API Key must be set before sending a request.'), 'error');
      return;
    }

    // Set Object Properties :: Concatinate Address Field(s) into String
    if (is_object($GoogleGeocoding)) {

      // Extract :: Field Values from Submitted Form
      $street_address = $form_state->getValue('street_address');
      $city = $form_state->getValue('city');
      $state = $form_state->getValue('state');
      $zip = $form_state->getValue('zip');

      // Concatinate :: Address for Geocoding request
      $GoogleGeocoding->setAddress($city . ',' . $city . ',' . $state . ' ' . $zip);

    } else {
      drupal_set_message($this->t('Unable to instantiate the Google Places API PHP Wrapper Object.'), 'error');
      return;
    }

    // Request :: Google Maps API Paginated Results
    $GoogleGeocodingResults = $GoogleGeocoding->executeAPICall();

    /* Extract Coordinates from Google Geocoding Response
    /* ------------------------------------------------------ */
    if (isset($GoogleGeocodingResults['result'])) {
      $latitude  = $GoogleGeocodingResults['result'][0]['geometry']['location']['lat'];
      $longitude = $GoogleGeocodingResults['result'][0]['geometry']['location']['lng'];

      $GooglePlacesAPI = new GooglePlacesAPI($google_places_api_key);
      $GooglePlacesAPI->setLocation($latitude . ',' . $longitude);

      // Extract :: Field Values from Submitted Form
      $keyword = $form_state->getValue('keyword');
      $type = $form_state->getValue('types');
      $ranked_by = $form_state->getValue('ranked_by');
      $radius = $form_state->getValue('radius');
      $pagination = $form_state->getValue('pagination');

      // Set Object Properties :: If Geocoding conditions have been met continue setting search proprties
      /* ------------------------------------------------------ */

      // Keyword :: 
      if (!empty($keyword)) {
        $GooglePlacesAPI->setKeyword(urlencode($keyword));
      }

      // Type :: 
      if (isset($type)) {
        $GooglePlacesAPI->setTypes($type);
      }

      // Ranked By :: 
      if (isset($ranked_by)) {
        $GooglePlacesAPI->setRankBy($ranked_by);
      }

      // Radius :: Default to 50,000 meters the highest possible radius
      if (!empty($radius)) {
        $GooglePlacesAPI->setRadius($radius);
      } else {
        $GooglePlacesAPI->setRadius(50000);
      }

    }

    if (true) {
      $GooglePlacesAPIResults = $GooglePlacesAPI->nearbySearch();
    }

    $form_state->setStorage($GooglePlacesAPIResults);
    $form_state->setRebuild(TRUE);

  }

  /**
   * Request Next Page
   *
   * WARNING :: Function is being developed, so please ignore for now.
   *
   * @param array $parent_request
   *   The reference parent request
   * @param $request_token
   *   The "next_page_token" returned by Google Places API
   * @param $request_count
   *   Current request number
   * @param $request_token
   *   Total numbr of pagination requests
   */
  public function requestNextPage(&$request_parent, $request_token, $request_count, $request_total) {

    if ( $count <= $total ) {
      $GooglePlacesAPIPagination = new GooglePlacesAPI($google_places_api_key);
      $GooglePlacesAPIPaginationResult = $GooglePlacesAPIResultsPagination->repeat();
      requestNextPage($request_parent, $GooglePlacesAPIPaginationResult['next_page_token'], $count, $total);
    }

  }

}
